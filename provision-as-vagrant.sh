#!/bin/bash

######################################################################
# This is a script for provisioning a "xenial" vagrant environment #
######################################################################

function announce() {
    printf "\n \n \n     ================\n"
    printf "     > $1 \n"
    printf "     ================\n \n \n "
}

#Node Version Manager
announce "Installing NVM..."
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#NodeJS
announce "Installing NodeJS via NVM..."
nvm install node

#SSH
eval "$(ssh-agent -s)"
ssh-add -k ~/.ssh/id_rsa
sudo service ssh restart

announce "FINISHED PROVISIONING AS VAGRANT"
