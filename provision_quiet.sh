#!/bin/bash

######################################################################
# This is a script for provisioning a "xenial" vagrant environment #
######################################################################

printf "\n \n \n \n==================================\n INSTALLING WEB DEVELOPMENT STACK \n==================================\n \n"

#apt-get
printf "Updating and upgrading apt-get..."
apt-get -y update > /dev/null 2>&1
apt-get -y upgrade > /dev/null 2>&1
printf "apt-get updated and upgraded!\n \n"

#Apache
printf "Installing Apache2..."
apt-get install -y apache2 > /dev/null 2>&1
printf "Apache2 installed!\n \n"

#Rewrite module
printf "Enabling Apache2 rewrite module..."
a2enmod rewrite > /dev/null 2>&1
printf "Restarting Apache2 for module to be loaded..."
service apache2 restart > /dev/null 2>&1
printf "Rewrite module enabled!\n \n"

#MySQL
printf "Installing MySQL..."
debconf-set-selections <<< 'mysql-server mysql-server/root_password password pass123'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password pass123'
apt-get -y install mysql-server > /dev/null 2>&1
printf "MySQL installed!\n \n"

#SQLite
printf "Installing SQLite..."
apt-get install -y sqlite3
printf "SQLite installed!\n \n"

#PHP
printf "Installing PHP..."
apt-get install software-properties-common > /dev/null 2>&1
add-apt-repository ppa:ondrej/php > /dev/null 2>&1
apt-get update > /dev/null 2>&1
apt-get install -y php7.2 > /dev/null 2>&1
printf "PHP installed!\n \n"

#PHP extensions
printf "Installing PHP extensions..."
apt-get install -y php-pear php7.2-curl php7.2-dev php7.2-gd php7.2-mbstring php7.2-zip php7.2-mysql php7.2-xml php7.2-sqlite3 > /dev/null 2>&1
printf "PHP extensions installed!\n \n"

#XDebug
printf "Installing XDebug..."
apt-get install -y php-xdebug > /dev/null 2>&1
printf "Restarting Apache2 for XDebug to be loaded..."
service apache2 restart > /dev/null 2>&1
printf "XDebug installed!\n \n"

#Composer
printf "Installing Composer..."
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" > /dev/null 2>&1
php -r "if (hash_file('sha384', 'composer-setup.php') === 'baf1608c33254d00611ac1705c1d9958c817a1a33bce370c0595974b342601bd80b92a3f46067da89e3b06bff421f182') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" > /dev/null 2>&1
php composer-setup.php > /dev/null 2>&1
php -r "unlink('composer-setup.php');" > /dev/null 2>&1
mv composer.phar /usr/local/bin/composer > /dev/null 2>&1
printf "Composer installed!\n \n"

#NodeJS
printf "Installing NodeJS..."
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash - > /dev/null 2>&1
apt-get install -y nodejs > /dev/null 2>&1
printf "NodeJS installed!\n \n"

#MongoDB
printf "Installing MongoDB..."
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 > /dev/null 2>&1
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list > /dev/null 2>&1
apt-get update > /dev/null 2>&1
apt-get install -y mongodb-org > /dev/null 2>&1
systemctl start mongod > /dev/null 2>&1
systemctl enable mongod > /dev/null 2>&1
printf "MongoDB Installed!"

#Redis
printf "Installing Redis..."
apt-get install -y redis-server > /dev/null 2>&1
printf "Redis installed!\n \n"

#Git
printf "Installing Git..."
apt-get install -y git > /dev/null 2>&1
printf "Git installed!\n \n"

#Zip and unzip
printf "Installing zip and unzip"
apt-get install -y zip > /dev/null 2>&1
apt-get install -y unzip > /dev/null 2>&1
printf "zip and unzip installed!\n \n"

printf "================\nFINISHED\n================\n"
