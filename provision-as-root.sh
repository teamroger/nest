#!/bin/bash

######################################################################
# This is a script for provisioning a "xenial" vagrant environment #
######################################################################

function announce() {
    printf "\n \n \n     ================\n"
    printf "     > $1 \n"
    printf "     ================\n \n \n "
}

announce "INSTALLING WEB DEVELOPMENT STACK"

#apt-get
announce "Updating and upgrading apt-get..."
apt-get -y update
apt-get -y upgrade

#Git
announce "Installing Git..."
apt-get install -y git

#Apache
announce "Installing Apache2..."
apt-get install -y apache2

#Rewrite module
announce "Enabling Apache2 rewrite module..."
a2enmod rewrite
announce "Restarting Apache2 for module to be loaded..."
service apache2 restart

#MySQL
announce "Installing MySQL..."
debconf-set-selections <<< 'mysql-server mysql-server/root_password password pass123'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password pass123'
apt-get -y install mysql-server

#SQLite
announce "Installing SQLite..."
apt-get install -y sqlite3

#PHP
announce "Installing PHP..."
apt-get install software-properties-common
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y php7.3

#PHP extensions
announce "Installing PHP extensions..."
apt-get install -y php-pear php7.3-curl php7.3-dev php7.3-gd php7.3-mbstring php7.3-zip php7.3-mysql php7.3-xml php7.3-sqlite3

#XDebug
announce "Installing XDebug..."
apt-get install -y php7.3-xdebug
announce "Restarting Apache2 for XDebug to be loaded..."
service apache2 restart

#Composer
announce "Installing Composer..."
EXPECTED_CHECKSUM="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid installer checksum'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
rm composer-setup.php
sudo mv composer.phar /usr/local/bin/composer

echo "" >> ~/.bashrc
echo "export PATH=$PATH:/home/vagrant/.config/composer/vendor/bin" >> ~/.bashrc

#MongoDB
announce "Installing MongoDB..."
apt-get install gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
apt-get update
apt-get install -y mongodb-org
systemctl daemon-reload
systemctl start mongod
systemctl enable mongod

#Redis
announce "Installing Redis..."
apt-get install -y redis-server

#Zip and unzip
announce "Installing zip and unzip..."
apt-get install -y zip
apt-get install -y unzip

#SSH
announce "Setting up SSH key..."
cp /projects/keys/linux/id_rsa /home/vagrant/.ssh/
chmod 400 /home/vagrant/.ssh/id_rsa

echo 'eval "$(ssh-agent -s)"' >> /home/vagrant/.bashrc
echo 'ssh-add -k ~/.ssh/id_rsa' >> /home/vagrant/.bashrc
echo 'sudo service ssh restart' >> /home/vagrant/.bashrc

announce "FINISHED PROVISIONING AS ROOT"
